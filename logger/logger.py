import logging
import sys
import logstash
import os

class LoggerFactory(object):
    _LOG = None

    @staticmethod
    def __create_logger(logger_name, log_level, log_file, enable_logstash):
        console_format = logging.Formatter('%(name)s : %(funcName)s : %(levelname)s : %(message)s')
        file_format = logging.Formatter('%(asctime)s : %(name)s : %(funcName)s : %(levelname)s : %(message)s')

        # Override logging level if environment variable is set
        if os.getenv("LOG_LEVEL"):
            if os.getenv("LOG_LEVEL") == "INFO":
                log_level = logging.INFO
            elif os.getenv("LOG_LEVEL") == "ERROR":
                log_level = logging.ERROR
            elif os.getenv("LOG_LEVEL") == "DEBUG":
                log_level = logging.DEBUG

        LoggerFactory._LOG = logging.getLogger(logger_name)
        LoggerFactory._LOG.setLevel(log_level)
        
        consolelog = logging.StreamHandler(sys.stdout)
        consolelog.setFormatter(console_format)
        consolelog.setLevel(log_level)
        LoggerFactory._LOG.addHandler(consolelog)

        # Support enabling logstash and file logging with an environment variable
        if os.getenv("ENABLE_LOGSTASH") == "true":
            enable_logstash = True

        if enable_logstash:
            logstash_host=os.getenv("LOGSTASH_HOST", "localhost")
            logstash_port=os.getenv("LOGSTASH_PORT", 32603)
            logstashlog = logstash.TCPLogstashHandler(logstash_host, logstash_port, version=1)
            logstashlog.setLevel(log_level)
            LoggerFactory._LOG.addHandler(logstashlog)
        
        if (log_file != None):
            filelog = logging.FileHandler(log_file)
            filelog.setFormatter(file_format)
            filelog.setLevel(log_level)
            LoggerFactory._LOG.addHandler(filelog)
        
        return LoggerFactory._LOG

    @staticmethod
    def get_logger(logger_name, log_level=logging.INFO, log_file=None, enable_logstash=False):
        """
        A static method called by other modules to initialize logger in
        their own module
        """
        logger = LoggerFactory.__create_logger(logger_name, log_level, log_file, enable_logstash)
        
        # return the logger object
        return logger
