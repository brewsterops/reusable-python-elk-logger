from logger import LoggerFactory

log = LoggerFactory.get_logger("PythonTestLogger", log_file='TestLogger.log')

def main():

    log.info("Test Message 1")
    log.info("EventsAlarms: Test Event 1")
    log.info("EventsAlarms: Test Event 2")

if __name__ == "__main__":
    main()